import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cloud-view',
  templateUrl: './cloud-view.component.html',
  styleUrls: ['./cloud-view.component.css']
})
export class CloudViewComponent implements OnInit {
  doc: any = '';
  urls: string= ''
  viewUrls: string= ''

  constructor() { }

  ngOnInit(): void {
  }
  
  setValue() {
    this.urls = this.doc
    if(this.urls.includes('google')) {
      this.viewUrls = `https://docs.google.com/gview?url=%${this.urls}%&embedded=true`
    }
  }

}
