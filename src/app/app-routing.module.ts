import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { CloudViewComponent } from './cloud-view/cloud-view.component';
import { MeetingComponent } from './meeting/meeting.component';

const routes: Routes = [
  { path: 'cloud-viewer', component: CloudViewComponent },
  { path: 'meeting', component: MeetingComponent },
  { path: '*', component: AppComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
