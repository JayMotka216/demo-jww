import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import * as OT from '@opentok/client';

@Component({
  selector: 'app-meeting',
  templateUrl: './meeting.component.html',
  styleUrls: ['./meeting.component.css']
})
export class MeetingComponent implements OnInit {
  roomname: any = '';
  username: any = '';
  type: any = 'person';
  connectionCount: number = 0
  session!: OT.Session;
  publisher!: OT.Publisher;
  audioMode: boolean = false
  videoMode: boolean = false
  audioInputDevices: any;
  videoInputDevices: any;
  data: any;
  currentStreams: any[] = [];
  
  constructor(private httpController: HttpClient) { }

  ngOnInit(): void {
    this.getDevice()
  }
  
  // first publish the stream and then subscribe to the stream
  
  async publishStream(session: OT.Session, id: string, videoSource?: any) {
    
    const pubOption: OT.PublisherProperties = {
      audioSource: this.audioInputDevices && this.type === "person"  ? this.audioInputDevices[0]?.deviceId : null,//&& 
      videoSource: videoSource,
      // videoSource: this.type,
      // maxResolution: { width: 1920, height: 1080 },
      width: this.type === 'screen' ? 1000 : 350,
      height: this.type === 'screen' ? 1000 : 350,
      mirror: false,
      // usePreviousDeviceSelection: true, //use pre selected device
      name: this.username,
    }
    
    const publisher = OT.initPublisher(id, pubOption, (error: any) => {
      if(error) console.log('publisher error', error);
    })
    return await session.publish(publisher, (error: any) => {
      if(error) throw error
      console.log('session published');
      console.log('session pub data', JSON.parse((<string>session.connection?.data)));
      console.log('session pub', session);
    });
  }
  
  async subscribeStream(session: OT.Session) {
    await session.on('streamCreated', async (event: any) => {
      console.log('event',event);
      const subOptions: OT.SubscriberProperties = {
        style: { nameDisplayMode: "on" },
        // fitMode: 'contain',
        insertMode : "replace",
        height: "100%",
        width: "100%"
      }
      console.log('session',session);
      // let subs = []
      // var se = [1, ...session.getSubscribersForStream(event.stream)]
      // subs = se.map(async (data: any) => {
        console.log('session sub data', JSON.parse((<string>session.connection?.data)));
        console.log(event.stream.videoType);
        
        if(event.stream.videoType === 'screen') {
          return await session.subscribe(event.stream, 'screens', subOptions, (error) => {
            if(error) throw error
            this.type = 'person'
          });
        } else {
          const id = `subscriber${this.connectionCount}`
          this.connectionCount++
          let div = (<HTMLDivElement>document.createElement('div'));
          div.style.border = "5px solid red;"
          div.style.borderRadius = "5px;"
          div.id = id;
          (<HTMLDivElement>document.getElementById('subscribers')).appendChild(div);
          return await session.subscribe(event.stream, id, subOptions, (error) => {
            if(error) throw error
          });
        }
        // })r
        
        // console.log('subscribers', subs);
        // return subs
      })
    }
    
    async connect(data: any, id: string, type?: string) {
      if(this.session.connection?.connectionId || type === "screen") {
        this.publisher = await this.publishStream(this.session, id, type)
        console.log('publisher',this.publisher);
      } else {
        await this.session.connect(data.token, async (error: any) => {
          if (error) throw error
          console.log('connection established!');
          this.publisher = await this.publishStream(this.session, id, this.videoInputDevices[0]?.deviceId)
          console.log('publisher',this.publisher);
        })
      }
      
      await this.subscribeStream(this.session)
    }
    
    startCall() {
      this.httpController.post<any>(
        `http://localhost:3000/session/${this.roomname}`, { username: this.username })
        .subscribe({
          next: async (data: any) => {
            this.type = "person"
            this.data = data
            this.session = OT.initSession(data.apiKey,data.sessionId)
            this.listnerMethods()
            await this.connect(data,'publishers', this.videoInputDevices[0]?.deviceId)
          },
          error: (error: any) => {
        console.log(error);
      }
    })
  }

  async takeSnap() {
    var imgData = await this.publisher.getImgData();
    console.log(imgData);
    
    var img = (<HTMLElement>document.createElement("img"));
    img.style.height = "150px"
    img.style.width = "150px"
    img.setAttribute("src", "data:image/png;base64," + imgData);
    
    // Replace with the parent DIV for the img
    (<HTMLElement>document.getElementById("snap")).appendChild(img);
  }
  
  disconnect() {
    this.session.disconnect()
  }
  
  listnerMethods() {
    this.session.on({
      streamCreated: (event: any) => {
        this.currentStreams.push({name: event?.stream?.name, streamId: event?.stream?.streamId})
      },
      connectionCreated: (event: any) => {
        if (event.connection.connectionId != this.session?.connection?.connectionId) {
          console.log(`Another client connected ${this.session?.sessionId} ${this.connectionCount} + total.`);
        }
      },
      connectionDestroyed: (event: any) => {
          this.connectionCount--;
          console.log(`A client disconnected. ${this.connectionCount} total.`);
      },
      sessionReconnecting: (event: any) => {
        // Display a user interface notification.
        console.log(event);
        
        console.log('session reconnecting');
      },
      sessionReconnected: (event: any) => {
        console.log('session resumed');
      },
      sessionDisconnected: (event: any) => {
        console.log('session disconnected');
        // Adjust user interface.
      }
    }, this.session);
  }

  getDevice() {
    OT.getDevices((error: any, devices: any) => {
      if(error) throw error
      this.audioInputDevices = devices.filter((element: any) => {
        return element.kind == "audioInput";
      });
      this.videoInputDevices = devices.filter((element: any) => {
        return element.kind == "videoInput";
      });
      for (var i = 0; i < this.audioInputDevices.length; i++) {
        console.log("audio input device: ", this.audioInputDevices[i]);
      }
      for (i = 0; i < this.videoInputDevices.length; i++) {
        console.log("video input device: ", this.videoInputDevices[i]);
      }
    });
  }

  audio() {
    this.audioMode = !this.audioMode
    console.log('audio mode', !this.audioMode);
    this.publisher.publishAudio(!this.audioMode)
  }
  video() {
    this.videoMode = !this.videoMode
    console.log('video mode', !this.videoMode);
    this.publisher.publishVideo(!this.videoMode)
  }

  checkStreams() {
    this.session.on("streamPropertyChanged", (event: any) => {
      var subscribers = this.session.getSubscribersForStream(event.stream);
      subscribers.forEach((sub: any, index: number) => {
        console.log(index, sub);
      })
    })
  }

  screenShare() {
    this.type = 'screen'
    this.connect(this.data,'screens', 'screen')
  }
  
}
